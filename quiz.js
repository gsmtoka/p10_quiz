// Select all the elements
const start = document.getElementById("start");
const quiz = document.getElementById("quiz");
const question = document.getElementById("question");
const qImg = document.getElementById("qImg");
const choiceA = document.getElementById("A");
const choiceB = document.getElementById("B");
const choiceC = document.getElementById("C");
const counter = document.getElementById("counter");
const timeGauge = document.getElementById("timeGauge");
const progress = document.getElementById("progress");
const scoreDiv = document.getElementById("scoreContainer");

// Create our questions
let questions = [
    {
        question : "რა მქვია მე?",
        imgSrc : "img/toko.jpg",
        choiceA : "თორნიკე",
        choiceB : "ვალერი",
        choiceC : "რაზმიკი",
        correct : "A"
    }, {
        question : "დაასახელეთ (დააკლიკეთ) ჩემი გვარი:",
        imgSrc : "img/toko.jpg",
        choiceA : "ხაჩატურიანი",
        choiceB : "ხუციშვილი",
        choiceC : "თანდილაური",
        correct : "B"
    }, {
        question : "რამდენს ვიწონი?",
        imgSrc : "img/toko.jpg",
        choiceA : "90 კგ",
        choiceB : "75 კგ",
        choiceC : "145 კგ",
        correct : "C"
    }, {
        question : "რა მოწოდების ვარ?",
        imgSrc : "img/toko.jpg",
        choiceA : "მძღოლი",
        choiceB : "ვებ-დევპი",
        choiceC : "ხაბაზი",
        correct : "B"
    }, {
        question : "მოვახერხებ თუ არა 2 წლის მერე crossover-ზე მუშაობას?",
        imgSrc : "img/toko.jpg",
        choiceA : "დიახ",
        choiceB : "ვერ",
        choiceC : "არ ვიცი",
        correct : "A"
    }
];

// Create some variables
const lastQuestion = questions.length - 1;
let runningQuestion = 0;
let count = 0;
const questionTime = 10; // 10s
const gaugeWidth = 150; // 150px
const gaugeUnit = gaugeWidth / questionTime;
let TIMER; 
let score = 0;

// Render a question
function renderQuestion() {
    let q = questions[runningQuestion];

    question.innerHTML = "<p>"+ q.question +"</p>";
    qImg.innerHTML = "<img src="+ q.imgSrc +">";
    choiceA.innerHTML = q.choiceA;
    choiceB.innerHTML = q.choiceB;
    choiceC.innerHTML = q.choiceC;
}

start.addEventListener("click", startQuiz);

// Start quiz
function startQuiz() {
    start.style.display = "none";
    renderQuestion();
    quiz.style.display = "block";
    renderProgress();
    renderCounter();
    TIMER = setInterval(renderCounter, 1000); // 1 second
}

// Render progress
function renderProgress() {
    for(let qIndex = 0; qIndex <= lastQuestion; qIndex++) {
        progress.innerHTML += "<div class='prog' id="+ qIndex +"></div>";
    }
}

// Counter render
function renderCounter() {
    if(count <= questionTime) {
        counter.innerHTML = count;
        timeGauge.style.width = count * gaugeUnit + "px";
        count++
    }else{
        count = 0;
        // change progress color to green
        answerIsWrong();
        if(runningQuestion < lastQuestion) {
            runningQuestion++;
            renderQuestion();
        }else{
            // End the quiz and show the score
            clearInterval(TIMER);
            scoreRender();
        }
    }
}

// Check Answer
function checkAnswer(answer) {
    if(answer == questions[runningQuestion].correct) {
        // answer is correct
        score++;
        // change progress color to green
        answerIsCorrect();
    }else{
        // answer is wrong
        // change progress color to red
        answerIsWrong();
    }
    count = 0;
    if(runningQuestion < lastQuestion) {
        runningQuestion++;
        renderQuestion();
    }else{
        // End the quiz and show the score
        clearInterval(TIMER);
        scoreRender();
    }
}

// Answer is correct
function answerIsCorrect() {
    document.getElementById(runningQuestion).style.backgroundColor = "#0f0";
}

// Answer is wrong
function answerIsWrong() {
    document.getElementById(runningQuestion).style.backgroundColor = "#f00";
}

// Score render
function scoreRender() {
    scoreDiv.style.display = "block";

    // Calculate the amount of question percent answered by the user
    const scorePerCent = Math.round(100 * score/questions.length);

    //Choose the image based on the scorePerCent
    let img =   (scorePerCent >= 80) ? "img/5.png" :
                (scorePerCent >= 60) ? "img/4.png" :
                (scorePerCent >= 40) ? "img/3.png" :
                (scorePerCent >= 20) ? "img/2.png" :
                "img/1.png";
    scoreDiv.innerHTML = "<img src="+ img +">"; 
    scoreDiv.innerHTML += "<p>"+ scorePerCent +"%</p>";
}